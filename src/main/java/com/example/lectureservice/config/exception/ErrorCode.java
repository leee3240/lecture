package com.example.lectureservice.config.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Getter
@AllArgsConstructor
public enum ErrorCode {

    /* 404 NOT_FOUND : Resource 를 찾을 수 없음 */
    NO_LECTURE_FOUND(NOT_FOUND, "강의 데이터가 존재하지 않습니다"),
    NO_SUBSCRIPTION_FOUND(NOT_FOUND, "강의 신청내역이 존재하지 않습니다"),

    /* 409 CONFLICT : Resource 의 현재 상태와 충돌. 보통 중복된 데이터 존재 */
    DUPLICATE_RESOURCE(CONFLICT, "데이터가 이미 존재합니다"),
    DUPLICATE_SUBSCRIPTION(CONFLICT, "강의 신청내역이 이미 존재합니다."),
    SUBSCRIPTION_LIMIT_OVER(CONFLICT, "수강신청 인원이 초과되었습니다.");

    private final HttpStatus httpStatus;
    private final String detail;

}
