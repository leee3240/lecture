package com.example.lectureservice.lecture;

import com.example.lectureservice.lecture.domain.Lecture;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "강의 등록 및 조회")
@RequestMapping("/api")
@RequiredArgsConstructor
@RestController
public class LectureController {

    private final LectureService lectureService;

    @ApiOperation("강의 등록")
    @PostMapping("/lecture")
    public ResponseEntity<?> saveLecture(@RequestBody Lecture lecture) {
        return ResponseEntity.ok(lectureService.saveLecture(lecture));
    }

    @ApiOperation("강의 목록조회")
    @GetMapping("/lecture")
    public ResponseEntity<?> getLectureList(@RequestParam(value = "isDeleted", defaultValue = "false", required = false) boolean isDeleted,
                                            @PageableDefault(size = 10) Pageable pageable){
        return ResponseEntity.ok(lectureService.getLectureList(isDeleted, pageable));
    }

    @ApiOperation("강의 단건조회")
    @GetMapping("/lecture/{idx}")
    public ResponseEntity<?> getLecture(@PathVariable long idx){
        return ResponseEntity.ok(lectureService.getLecture(idx));
    }

    @ApiOperation("강의 단건삭제")
    @DeleteMapping("/lecture/{idx}")
    public ResponseEntity<?> deleteLecture(@PathVariable long idx){
        return ResponseEntity.ok(lectureService.deleteLecture(idx));
    }




    @ApiOperation("사용자 강의 조회")
    @GetMapping("/public/lecture")
    public ResponseEntity<?> getPublicLectureList(@PageableDefault(size = 10) Pageable pageable){
        return ResponseEntity.ok(lectureService.getPublicLectureList(pageable));
    }

    @ApiOperation("사용자 강의 신청 목록조회")
    @GetMapping("/public/lecture/history")
    public ResponseEntity<?> getMyHistory(@RequestParam("sabun") long sabun,
                                          @PageableDefault(size = 10) Pageable pageable){
        return ResponseEntity.ok(lectureService.getMyHistory(sabun, pageable));
    }



}
