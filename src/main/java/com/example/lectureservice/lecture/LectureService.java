package com.example.lectureservice.lecture;

import com.example.lectureservice.config.exception.CustomException;
import com.example.lectureservice.config.exception.ErrorCode;
import com.example.lectureservice.lecture.domain.Lecture;
import com.example.lectureservice.lecture.domain.LectureSubscriptionDTO;
import com.example.lectureservice.lecture.repository.LectureRepository;
import com.example.lectureservice.subscription.domain.Subscription;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LectureService {

    private final LectureRepository lectureRepository;

    @Transactional
    public Lecture saveLecture(Lecture lecture){
        return lectureRepository.save(lecture);
    }

    public Page<Lecture> getLectureList(boolean isDeleted, Pageable pageable) {
        return lectureRepository.findAllByIsDeleted(isDeleted, pageable);
    }

    public Lecture getLecture(long idx) {
        Lecture lecture = lectureRepository.findById(idx).orElseThrow(() -> new CustomException(ErrorCode.NO_LECTURE_FOUND));
        List<Subscription> subscriptionList = lecture.getSubscriptions().stream().filter(s -> s.getIsDeleted().equals(false)).collect(Collectors.toList());
        lecture.setSubscriptions(subscriptionList);

        return lecture;
    }

    @Transactional
    public long deleteLecture(long idx) {
        Lecture optLecture = lectureRepository.findById(idx).orElseThrow(() -> new CustomException(ErrorCode.NO_LECTURE_FOUND));
        optLecture.setIsDeleted(false);

        return optLecture.getIdx();
    }

    public Page<Lecture> getPublicLectureList(Pageable pageable) {
        return lectureRepository.findAllByIsDeletedFalseAndTime(pageable);
    }

    public Page<LectureSubscriptionDTO> getMyHistory(long sabun, Pageable pageable) {
        return lectureRepository.findMyHistory(sabun, pageable);
    }

}
