package com.example.lectureservice.lecture.domain;

import com.example.lectureservice.subscription.domain.Subscription;
import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;


@Data
@Entity
@Table(name = "lecture")
@DynamicInsert
@DynamicUpdate
public class Lecture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idx;
    private String lecturer;
    private String place;
    private LocalDateTime lectureTime;
    private int headcount;
    @Column(columnDefinition = "TEXT")
    private String content;
    @ColumnDefault("0")
    private Boolean isDeleted;

    private Long createdBy;
    @Column(updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;
    private Long updatedBy;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name="lectureIdx", referencedColumnName = "idx", updatable = false, insertable = false)
    private List<Subscription> subscriptions;


}
