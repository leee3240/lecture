package com.example.lectureservice.lecture.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LectureSubscriptionDTO {

    private long lectureIdx;
    private String lecturer;
    private String place;
    private LocalDateTime lectureTime;

    private LocalDateTime subscriptionUpdatedAt;
    private Boolean subscriptionIsDeleted;

    public LectureSubscriptionDTO(long lectureIdx, String lecturer, String place, LocalDateTime lectureTime, LocalDateTime subscriptionUpdatedAt, Boolean subscriptionIsDeleted){
        this.lectureIdx = lectureIdx;
        this.lecturer = lecturer;
        this.place = place;
        this.lectureTime = lectureTime;
        this.subscriptionUpdatedAt = subscriptionUpdatedAt;
        this.subscriptionIsDeleted = subscriptionIsDeleted;
    }

}
