package com.example.lectureservice.lecture.repository;

import com.example.lectureservice.lecture.domain.Lecture;
import com.example.lectureservice.lecture.domain.LectureSubscriptionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LectureRepository extends JpaRepository<Lecture, Long> {

    Page<Lecture> findAllByIsDeleted(@Param("isDeleted") boolean isDeleted, Pageable pageable);

    @Query(value = "SELECT * " +
            "       FROM Lecture " +
            "       WHERE is_deleted = 0 " +
            "       AND NOW() <= DATE_ADD(lecture_time, INTERVAL 1 DAY) " +
            "       AND NOW() >= DATE_ADD(lecture_time, INTERVAL -7 DAY)"
            , nativeQuery = true)
    Page<Lecture> findAllByIsDeletedFalseAndTime(Pageable pageable);

    @Query(value = "SELECT new com.example.lectureservice.lecture.domain.LectureSubscriptionDTO(l.idx, l.lecturer, l.place, l.lectureTime, s.updatedAt, s.isDeleted) " +
            "       FROM Lecture l " +
            "       LEFT JOIN Subscription s ON l.idx = s.lectureIdx " +
            "       WHERE s.sabun = :sabun")
    Page<LectureSubscriptionDTO> findMyHistory(@Param("sabun") long sabun, Pageable pageable);


}
