package com.example.lectureservice.subscription;

import com.example.lectureservice.subscription.domain.Subscription;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "수강신청/취소")
@RequestMapping("/api/subscription")
@RequiredArgsConstructor
@RestController
public class SubscriptionController {

    private final SubscriptionService subscriptionService;

    @ApiOperation("수강신청")
    @PostMapping
    public ResponseEntity<?> saveSubscription(@RequestBody Subscription subscription) {
        return ResponseEntity.ok(subscriptionService.saveSubscription(subscription));
    }

    @ApiOperation("수강신청 취소")
    @DeleteMapping("/{lectureIdx}")
    public ResponseEntity<?> deleteSubscription(@PathVariable("lectureIdx") long lectureIdx,
                                                @RequestParam("sabun") long sabun){
        return ResponseEntity.ok(subscriptionService.deleteSubscription(lectureIdx, sabun));
    }

}
