package com.example.lectureservice.subscription;

import com.example.lectureservice.config.exception.CustomException;
import com.example.lectureservice.config.exception.ErrorCode;
import com.example.lectureservice.lecture.domain.Lecture;
import com.example.lectureservice.lecture.repository.LectureRepository;
import com.example.lectureservice.subscription.domain.Subscription;
import com.example.lectureservice.subscription.repository.SubscriptionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SubscriptionService {

    private final LectureRepository lectureRepository;
    private final SubscriptionRepository subscriptionRepository;

    @Transactional
    public Subscription saveSubscription(Subscription param){
        long lectureIdx = param.getLectureIdx();
        long sabun = param.getSabun();

        //존재하는 강의인지 확인
        Lecture lecture = lectureRepository.findById(lectureIdx).orElseThrow(() -> new CustomException(ErrorCode.NO_LECTURE_FOUND));

        //신청했던 내역이 있는지 확인
        Optional<Subscription> optSubscription = subscriptionRepository.findByLectureIdxAndSabun(lectureIdx, sabun);
        if(optSubscription.isPresent() && !optSubscription.get().getIsDeleted()) {
            //신청한 강의일 경우
            throw new CustomException(ErrorCode.DUPLICATE_SUBSCRIPTION);
        }

        //제한인원 확인
        long subscriptionCount = subscriptionRepository.countByLectureIdxAndIsDeletedIsFalse(lecture.getIdx());
        if(lecture.getHeadcount() == subscriptionCount) {
            throw new CustomException(ErrorCode.SUBSCRIPTION_LIMIT_OVER);
        }

        if(optSubscription.isPresent() && optSubscription.get().getIsDeleted()) {
            //삭제한 신청 내역이 있으면 재신청
            optSubscription.get().setIsDeleted(false);
            return optSubscription.get();
        }else {
            Subscription subscription = new Subscription();
            subscription.setLectureIdx(lectureIdx);
            subscription.setSabun(sabun);
            return subscriptionRepository.save(subscription);
        }
    }

    @Transactional
    public long deleteSubscription(long idx, long sabun) {
        Subscription optSubscription = subscriptionRepository.findByLectureIdxAndSabun(idx, sabun).orElseThrow(() -> new CustomException(ErrorCode.NO_SUBSCRIPTION_FOUND));
        optSubscription.setIsDeleted(true);

        return optSubscription.getIdx();
    }


}
