package com.example.lectureservice.subscription.domain;

import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "subscription")
@DynamicUpdate
@DynamicInsert
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idx;
    private long lectureIdx;
    private long sabun;
    @ColumnDefault("0")
    private Boolean isDeleted;

    @Column(updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

}
