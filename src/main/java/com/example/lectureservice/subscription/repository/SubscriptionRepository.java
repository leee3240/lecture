package com.example.lectureservice.subscription.repository;

import com.example.lectureservice.subscription.domain.Subscription;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface SubscriptionRepository extends CrudRepository<Subscription, Long> {

    long countByLectureIdxAndIsDeletedIsFalse(long lectureId);

    Optional<Subscription> findByLectureIdxAndSabun(long lectureId, long sabun);

}
