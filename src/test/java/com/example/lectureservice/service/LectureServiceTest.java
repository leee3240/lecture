package com.example.lectureservice.service;

import com.example.lectureservice.lecture.LectureService;
import com.example.lectureservice.lecture.domain.Lecture;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@SpringBootTest
@Transactional
public class LectureServiceTest {

    @Autowired
    LectureService lectureService;

    @Test
    void 강의등록() {

        Lecture lecture = new Lecture();
        lecture.setLecturer("강사");
        lecture.setPlace("place");
        lecture.setHeadcount(15);
        lecture.setContent("it is test.");
        lecture.setLectureTime(LocalDateTime.now());

        lectureService.saveLecture(lecture);
    }

    @Test
    void 강의수정() {
        Lecture lecture = lectureService.getLecture(1);
        lecture.setContent("테스트");

        lectureService.saveLecture(lecture);
    }

    @Test
    void 강의삭제() {
        lectureService.deleteLecture(1);
    }

    @Test
    void 강의목록조회() {
        lectureService.getLectureList(false, Pageable.unpaged());
    }

    @Test
    void 강의상세조회() {
        lectureService.getLecture(1);
    }

    @Test
    void 수강신청내역조회() {
        lectureService.getMyHistory(22222, Pageable.unpaged());
    }

    @Test
    void 화면단수강목록조회() {
        lectureService.getPublicLectureList(Pageable.unpaged());
    }

}
