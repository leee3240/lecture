package com.example.lectureservice.service;

import com.example.lectureservice.subscription.SubscriptionService;
import com.example.lectureservice.subscription.domain.Subscription;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;

@SpringBootTest
@Transactional
public class SubscriptionServiceTest {

    @Autowired
    SubscriptionService subscriptionService;

    @Test
    void 수강신청() {

        Subscription subscription = new Subscription();
        subscription.setLectureIdx(2);
        subscription.setSabun(12345);

        subscriptionService.saveSubscription(subscription);
    }

    @Test
    void 수강신청취소() {
        subscriptionService.deleteSubscription(1, 12345);
    }

}
